#!/usr/bin/python
# -*- coding=utf-8 -*-

import sys
import os
sys.path.append(os.path.dirname(__file__))
import bookdb



body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Ksiazki</title>
    </head>
    <body>
        %s
    </body>
</html>
"""

def app(environ, start_response):
    status = '200 OK'
    c = ''
    d = str(environ.get('QUERY_STRING'))

    if d == '':
        c = 'Spis:<br>'
        for itemBooks in bookdb.BookDB().titles():
            c += "<a href='?id=" + itemBooks['id'] + "'>" + itemBooks['title'] + "</a><br>"
    elif 'id=' in d and '&' not in d:
        try:
            bookInfo = bookdb.BookDB().title_info(d.split('=')[1])
            c = "Autor: " + bookInfo['author'] + "<br>Tytul: " + bookInfo['title'] +"<br>ISBN: " + bookInfo['isbn'] + "<br>Wydawca: " + bookInfo['publisher']
        except:
            c = 'Error'

    response_body = body % (c)
    response_headers = [('Content-Type', 'text/html'),('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]

if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    server = make_server('localhost', 6644, app)
    server.serve_forever()
